#include "configuration.h"
#include "stm32f10x.h"
#include "rcc.h"
#include "usart.h"
#include "string.h"
#include "io.h"

int DAC_value;

int PWM_frequency;
int PWM_duty_cycle;

int counter_of_rgb_led_updates;

void provide_configuration(void);
void process_input(char input);
bool increase_DAC_value(int step);
bool decrease_DAC_value(int step);
bool increase_PWM_frequency_value(int step);
bool decrease_PWM_frequency_value(int step);
bool increase_PWM_duty_cycle_value(int step);
bool decrease_PWM_duty_cycle_value(int step);
void delay(vu32 nCount);
float calculate_temperature(float V_sensor);
float map(float value, float src_range_min, float src_range_max, float dst_range_min, float dst_range_max);
void write_into_shift_register(unsigned char data);
void test_RGB_LED(void);
void set_RGB_LED_color(float voltage_percentage);

void SystemInit(void)
{
	
}

int main(void)
{
	{
		counter_of_rgb_led_updates = 0;
		DAC_value = 0;
		PWM_frequency = 0;
		PWM_duty_cycle = 0;
	}
	
	provide_configuration();
	
	#ifdef USE_UART
	{
		USART_send_string(USART_ENGAGED, GREETING_S);
		USART_send_string(USART_ENGAGED, NL_CR_S);
	}
	#endif
	
	{
		unsigned int phototransistor_blocked_counter = 0;
		unsigned long long int ticks;
		bool user_output_auto_mode = TRUE;
		// A variable for temporary values. It's not possible to declare this in the while loop block.
		int tmp;
		
		unsigned int ADC2_prev_value = 0;
		
		{
			ADC2_prev_value = analog_input1(2, 0x04);
			
			if (ADC2_prev_value > PHOTOTRANSISTOR_BLOCKED_THRESHOLD)
			{
				phototransistor_blocked_counter++;
			}
		}		
		
		{
			write_into_shift_register(RGB_LED_OFF);
			test_RGB_LED();
		}
		
		while (1)
		{						
			unsigned int ADC1_value = analog_input1(1, 0x04);
			unsigned int ADC2_value = analog_input1(2, 0x04);
			unsigned int ADC3_value = analog_input1(3, 0x03);			
			unsigned int ADC6_value = analog_input1(6, 0x03);
			float V_sensor = 0;
			unsigned int potentiometer_value = ADC6_value < POTENTIOMETER_MIN ? POTENTIOMETER_MIN : ADC6_value;	
			
			{
				if (ADC2_value > PHOTOTRANSISTOR_BLOCKED_THRESHOLD && ADC2_prev_value <= PHOTOTRANSISTOR_BLOCKED_THRESHOLD)
				{
					phototransistor_blocked_counter++;
				}
			}
			
			{
				V_sensor = ((((float)(ADC3_value)) / 4096.0f)) * 3.3f;
			}
			
			{
				if (ticks % RGB_LED_UPDATE_FREQUENCY == 0)
				{
					unsigned int potentiometer_ramapped_value = map(potentiometer_value, POTENTIOMETER_MIN, POTENTIOMETER_MAX, 0.0f, 4095.0f);
					
					set_RGB_LED_color((float)(potentiometer_ramapped_value) / (float)(ADC_VALUE_MAX));
				}
			}
			
			ADC2_prev_value = ADC2_value;
			
			// Output for the user.
			#ifdef USE_UART
			{
				uint8_t data_byte;
				
				{					
					if (USART_get_received_data(USART_ENGAGED, &data_byte))
					{
						USART_send_string(USART_ENGAGED, ECHO_S);
						USART_send_byte(USART_ENGAGED, data_byte);
						USART_send_string(USART_ENGAGED, NL_CR_S);
						
						process_input(data_byte);
					}
				}
				
				if (data_byte == CHANGE_OUTPUT_MODE_SYMBOL)
				{
					user_output_auto_mode = (bool)(1 - (int)user_output_auto_mode);
				}
				
				if ((ticks % OUTPUT_FREQUENCY == 0 && user_output_auto_mode) || (data_byte == PRINT_STATE_SYMBOL))
				{		
					char ullong_as_string[ULLONG_MAX_LENGTH + 1];
					
					USART_send_string(USART_ENGAGED, START_OF_OUTPUT_S);
					
					#ifdef PRINT_TICKS
					ullong2string(ticks, ullong_as_string);
					USART_send_string(USART_ENGAGED, TICKS_S);
					USART_send_string(USART_ENGAGED, ullong_as_string);
					USART_send_string(USART_ENGAGED, NL_CR_S);
					#endif // PRINT_TICKS
						
					ullong2string((int)(((float)ADC1_value / (float)ADC_VALUE_MAX) * 100.0f), ullong_as_string);
					USART_send_string(USART_ENGAGED, LIGHTING_S);
					USART_send_string(USART_ENGAGED, ullong_as_string);
					USART_send_string(USART_ENGAGED, "%");
					USART_send_string(USART_ENGAGED, NL_CR_S);
					
					ullong2string(ADC1_value, ullong_as_string);
					USART_send_string(USART_ENGAGED, ADC1_S);
					USART_send_string(USART_ENGAGED, ullong_as_string);
					USART_send_string(USART_ENGAGED, NL_CR_S);
					
					ullong2string(ADC2_value, ullong_as_string);
					USART_send_string(USART_ENGAGED, ADC2_S);
					USART_send_string(USART_ENGAGED, ullong_as_string);
					USART_send_string(USART_ENGAGED, NL_CR_S);
					
					ullong2string(ADC3_value, ullong_as_string);
					USART_send_string(USART_ENGAGED, ADC3_S);
					USART_send_string(USART_ENGAGED, ullong_as_string);
					USART_send_string(USART_ENGAGED, NL_CR_S);
					
					ullong2string(ADC6_value, ullong_as_string);
					USART_send_string(USART_ENGAGED, ADC6_S);
					USART_send_string(USART_ENGAGED, ullong_as_string);
					USART_send_string(USART_ENGAGED, NL_CR_S);
					
					ullong2string(DAC_value, ullong_as_string);
					USART_send_string(USART_ENGAGED, DAC1_S);
					USART_send_string(USART_ENGAGED, ullong_as_string);
					USART_send_string(USART_ENGAGED, NL_CR_S);
					
					ullong2string(phototransistor_blocked_counter, ullong_as_string);
					USART_send_string(USART_ENGAGED, PHOTOTRANSISTOR_BLOCKED_S);
					USART_send_string(USART_ENGAGED, ullong_as_string);
					USART_send_string(USART_ENGAGED, NL_CR_S);			

					int2string((int)(map(potentiometer_value, POTENTIOMETER_MIN, POTENTIOMETER_MAX, -130.0f, 130.0f)), ullong_as_string);
					USART_send_string(USART_ENGAGED, ANGLE_S);
					USART_send_string(USART_ENGAGED, ullong_as_string);
					USART_send_string(USART_ENGAGED, NL_CR_S);	
					
					tmp = (int)(V_sensor * 1000.0f);
					ullong2string(tmp, ullong_as_string);
					USART_send_string(USART_ENGAGED, VOLTAGE_SENSOR_S);
					USART_send_string(USART_ENGAGED, ullong_as_string);
					USART_send_string(USART_ENGAGED, NL_CR_S);
					
					// Calculate the temperature and subtract 25 from it and multiply by 1000 in order to print a decimal value.
					// We get only the decimal part of the number.
					// It may consist of 3 digits at maximum.
					tmp = (int)((calculate_temperature(V_sensor) - 25.0f) * 1000.0f);
					USART_send_string(USART_ENGAGED, TEMPERATURE_S);
					ullong2string(25, ullong_as_string);
					USART_send_string(USART_ENGAGED, ullong_as_string);
					USART_send_string(USART_ENGAGED, ".");	
					// If the decimal part contains less than 3 digits, print 0.
					if (tmp < 100)
					{
						ullong2string(0, ullong_as_string);
						USART_send_string(USART_ENGAGED, ullong_as_string);
					}
					// If the decimal part contains less than 2 digits, print 0.
					if (tmp < 10)
					{
						ullong2string(0, ullong_as_string);
						USART_send_string(USART_ENGAGED, ullong_as_string);
					}
					ullong2string(tmp, ullong_as_string);
					USART_send_string(USART_ENGAGED, ullong_as_string);
					USART_send_string(USART_ENGAGED, NL_CR_S);
					
					ullong2string(PWM_frequency, ullong_as_string);
					USART_send_string(USART_ENGAGED, PWM_FREQUENCY_S);
					USART_send_string(USART_ENGAGED, ullong_as_string);
					USART_send_string(USART_ENGAGED, NL_CR_S);
					
					ullong2string(PWM_duty_cycle, ullong_as_string);
					USART_send_string(USART_ENGAGED, PWM_DUTY_CYCLE_S);
					USART_send_string(USART_ENGAGED, ullong_as_string);
					USART_send_string(USART_ENGAGED, NL_CR_S);
					
					USART_send_string(USART_ENGAGED, END_OF_OUTPUT_S);
				}
			}
			#endif // USE_UART
			
			analog_output1(DAC_value);
			
			ticks++;
		}
	}
}

void provide_configuration(void)
{
	RCC_configuration(); 
	GPIO_configuration();
	#ifdef USE_UART
	USART_configuration(USART_ENGAGED);
	#endif // USE_UART
	DAC1_configuration();
	ADC1_configuration();
	TIM3_configuration();
}

void process_input(char input)
{
	switch (input)
	{
		case INCREASE_DAC_SYMBOL:
		{
			increase_DAC_value(DAC_VALUE_USER_STEP);
		} break;
		
		case DECREASE_DAC_SYMBOL:
		{
			decrease_DAC_value(DAC_VALUE_USER_STEP);
		} break;
		
		case INCREASE_PWM_FREQUENCY_SYMBOL:
		{
			increase_PWM_frequency_value(PWM_FREQUENCY_STEP);
		} break;			
		
		case DECREASE_PWM_FREQUENCY_SYMBOL:
		{
			decrease_PWM_frequency_value(PWM_FREQUENCY_STEP);
		} break;	

		case INCREASE_PWM_DUTY_CYCLE_SYMBOL:
		{
			increase_PWM_duty_cycle_value(PWM_DUTY_CYCLE_STEP);
		} break;			
		
		case DECREASE_PWM_DUTY_CYCLE_SYMBOL:
		{
			decrease_PWM_duty_cycle_value(PWM_DUTY_CYCLE_STEP);
		} break;
		
		default:
		{
			
		}
	}
}

bool increase_DAC_value(int step)
{
	DAC_value += step;
	
	if (DAC_value > DAC_VALUE_MAX)
	{
		DAC_value = DAC_VALUE_MAX;
		
		return FALSE;
	}
	
	return TRUE;
}

bool decrease_DAC_value(int step)
{
	DAC_value -= step;
	
	if (DAC_value < DAC_VALUE_MIN)
	{
		DAC_value = DAC_VALUE_MIN;
		
		return FALSE;
	}
	
	return TRUE;
}

bool increase_PWM_frequency_value(int step)
{
	PWM_frequency += step;
	if (PWM_frequency > PWM_FREQUENCY_MAX)
	{
		PWM_frequency = PWM_FREQUENCY_MAX;
		
		return FALSE;
	}
			
	TIM3->ARR = PWM_frequency;
	
	return TRUE;
}

bool decrease_PWM_frequency_value(int step)
{
	PWM_frequency -= step;
	if (PWM_frequency < 0)
	{
		PWM_frequency = 0;
		
		return FALSE;
	}
			
	TIM3->ARR = PWM_frequency;
	
	return TRUE;
}

bool increase_PWM_duty_cycle_value(int step)
{
	PWM_duty_cycle += step;
	if (PWM_duty_cycle > PWM_DUTY_CYCLE_MAX)
	{
		PWM_duty_cycle = PWM_DUTY_CYCLE_MAX;
		
		return FALSE;
	}
			
	TIM3->CCR2 = PWM_duty_cycle;
	
	return TRUE;
}

bool decrease_PWM_duty_cycle_value(int step)
{
	PWM_duty_cycle -= step;
	if (PWM_duty_cycle < 0)
	{
		PWM_duty_cycle = 0;
		
		return FALSE;
	}
			
	TIM3->CCR2 = PWM_duty_cycle;
	
	return TRUE;
}

void delay(vu32 nCount)
{
	for (; nCount != 0; nCount--);
}

float calculate_temperature(float V_sensor)
{
	const float V_25 = 1.4f;
	const float avg_slope = 4.478f;
	float V = 0.0f;
	
	if (V_25 == V_sensor)
	{
		return 0.0f;
	}
		
	if (V_25 > V_sensor)
	{
		V = V_25 - V_sensor;
	}
	else
	{
		V = V_sensor - V_25;
	}
	
	V /= avg_slope;
	V += 25.0f;
	
	return V;
}

float map(float value, float src_range_min, float src_range_max, float dst_range_min, float dst_range_max)
{	
	return dst_range_min + ((dst_range_max - dst_range_min) / (src_range_max - src_range_min)) * (value - src_range_min);
}

void write_into_shift_register_reset(bool value)
{
	SHIFT_REGISTER_GATEWAY &= ~(1 << SHIFT_REGISTER_RESET_BIT);
	SHIFT_REGISTER_GATEWAY |= (value << SHIFT_REGISTER_RESET_BIT);
}

void write_into_shift_register_clock(bool value)
{
	SHIFT_REGISTER_GATEWAY &= ~(1 << SHIFT_REGISTER_CLOCK_BIT);
	SHIFT_REGISTER_GATEWAY |= (value << SHIFT_REGISTER_CLOCK_BIT);
}

void write_into_shift_register_data(bool value)
{
	SHIFT_REGISTER_GATEWAY &= ~(1 << SHIFT_REGISTER_DATA_BIT);
	SHIFT_REGISTER_GATEWAY |= (value << SHIFT_REGISTER_DATA_BIT);
}

void write_into_shift_register(unsigned char data)
{
	{
		write_into_shift_register_reset((bool)(0));
		write_into_shift_register_reset((bool)(1));
	}
	
	{
		int i;
		
		for (i = 0; i < 8; i++)
		{
			write_into_shift_register_data((bool)(data & 0x1));
			
			write_into_shift_register_clock((bool)(1));
			write_into_shift_register_clock((bool)(0));
			
			data = data >> 1;
		}
	}
}

void test_RGB_LED(void)
{
	write_into_shift_register(RGB_LED_R);
	delay(RGB_LED_TEST_DELAY);
	write_into_shift_register(RGB_LED_G);
	delay(RGB_LED_TEST_DELAY);
	write_into_shift_register(RGB_LED_B);
	delay(RGB_LED_TEST_DELAY);
	
	write_into_shift_register(RGB_LED_R RGB_LED_AND RGB_LED_G);
	delay(RGB_LED_TEST_DELAY);
	write_into_shift_register(RGB_LED_R RGB_LED_AND RGB_LED_B);
	delay(RGB_LED_TEST_DELAY);
	
	write_into_shift_register(RGB_LED_G RGB_LED_AND RGB_LED_B);
	delay(RGB_LED_TEST_DELAY);
}

void set_RGB_LED_color(float voltage_percentage)
{	
	if (voltage_percentage <= RGB_LED_R_MAX)
	{
		if (counter_of_rgb_led_updates % 2)
		{
			write_into_shift_register(RGB_LED_R);
		}
		else
		{
			write_into_shift_register(RGB_LED_OFF);
		}
	}
	else if (voltage_percentage > RGB_LED_R_MAX && voltage_percentage < RGB_LED_G_MIN)
	{
		write_into_shift_register(RGB_LED_R RGB_LED_AND RGB_LED_G);
	}
	else if (voltage_percentage >= RGB_LED_G_MIN && voltage_percentage <= RGB_LED_G_MAX)
	{
		if (counter_of_rgb_led_updates % 2)
		{
			write_into_shift_register(RGB_LED_G);
		}
		else
		{
			write_into_shift_register(RGB_LED_OFF);
		}
	}
	else if (voltage_percentage > RGB_LED_G_MAX && voltage_percentage < RGB_LED_B_MIN)
	{
		write_into_shift_register(RGB_LED_G RGB_LED_AND RGB_LED_B);
	}
	else
	{
		if (counter_of_rgb_led_updates % 2)
		{
			write_into_shift_register(RGB_LED_B);
		}
		else
		{
			write_into_shift_register(RGB_LED_OFF);
		}
	}
	
	counter_of_rgb_led_updates++;
}
