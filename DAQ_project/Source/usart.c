#include "usart.h"

/* USART initialization */
void USART_configuration(USART_TypeDef* USARTx)
{
	RCC->APB2ENR |= 0x4000;	
	
	/* Baud Rate setup 9600 Bd (see Reference manual for details) */  	
	USARTx->BRR = 0x09C4;

	/* 8 data bits */
	USARTx->CR1 &= 0xEFFF;      

	/* 1 stopbit */
	USARTx->CR2 &= 0xCFFF;      

	/* Parity disabled */
	USARTx->CR1 &= 0xFBFF;      

	/* Hardware flow control disable */
	USARTx->CR3 &= 0xFCFF;      
	
	/* Rx, TX enable */
	USARTx->CR1 |= 0x000C;      
	
	/* USART1 enable */
	USARTx->CR1 |= 0x2000;      
}

void USART_send_byte(USART_TypeDef* USARTx, uint16_t data)
{
	while ((USARTx->SR & 0x40) == 0)
	{
		
	}
	
	/* Transmit Data */
	USARTx->DR = (data & (uint16_t)0x01FF);
}

void USART_send_string(USART_TypeDef* USARTx, const char* string)
{	
	{
		int i = 0;
		
		while (string[i] != '\0')
		{
			USART_send_byte(USARTx, string[i++]);
		}
	}
}

bool USART_get_received_data(USART_TypeDef* USARTx, uint8_t* data_byte)
{
	if (USARTx->SR & 0x0020)
	{
		// Data recieved @ USART 
		*data_byte = (uint8_t) USARTx->DR;
		
		return TRUE;
	}
	
	return FALSE;
}
