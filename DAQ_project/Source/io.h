#ifndef ANALOG_IO_H
#define ANALOG_IO_H

#include "stm32f10x.h"

#define Pxy(gateway, gateway_n)		((gateway) & (1 << gateway_n))
#define PAx(gateway_n) 				Pxy(GPIOA->IDR, gateway_n)
#define PA0 						PAx(0)
#define PA1 						PAx(1)

#define DAC_VALUE_MAX 4095
#define DAC_VALUE_MIN 0

#define ADC_VALUE_MAX 4095

void GPIO_configuration(void);
void TIM3_configuration(void);
void DAC1_configuration(void);
void ADC1_configuration(void);
unsigned int analog_input1(unsigned char channel, unsigned char sample_time);
void analog_output1(int value);


#endif // ANALOG_IO_H
