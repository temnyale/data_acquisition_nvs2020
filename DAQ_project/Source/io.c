#include "io.h"

void GPIO_configuration(void)
{
	// PA.
	RCC->APB2ENR |= 1 << 2;
	// PB.
	RCC->APB2ENR |= 1 << 3;
	
	AFIO->MAPR = 0x04000000;
	
	// 5, 6, 7: PP output.
	GPIOB->CRL &= 0x000FFFFF; 
	GPIOB->CRL |= 0x11100000;
	
	// 9 (USART1 TX): alternate function output Push-pull, 10MHz.
	GPIOA->CRH &= 0xFFFFFF00;    
  	GPIOA->CRH |= 0x000000B0;

	// 10 (USART RX): floating input. 
	GPIOA->CRH &= 0xFFFFF0FF;    
  	GPIOA->CRH |= 0x00000400;

	// 7: for PWM.
	// The other for ADC.
	// 4 and 5 are reserved for DAC.
	GPIOA->CRL &= 0x00FF0000;
	GPIOA->CRL |= 0xB0000000;
}

void TIM3_configuration(void)
{
	RCC->APB1ENR |= 1 << 1;
	TIM3->ARR = 0;
	TIM3->PSC = 0x0;
	TIM3->CCMR1 |= 0x6868;
	TIM3->CCR1 = 5000;
	TIM3->CCR2 = 0;
	TIM3->CCER |= 0x11;
	TIM3->CR1 = 0x1;
}

void DAC1_configuration(void)
{
	// DACEN bit.
	RCC->APB1ENR |= 1 << 29;
	
	// Reset DAC.
	DAC->CR = 0;
	// BOFF1 bit.
	DAC->CR = 1 << 1;
	// EN1.
	DAC->CR = 1 << 0;
}

void ADC1_configuration(void)
{
	RCC->APB2ENR |= 0x200;	
	
	// Temperature sensor, ext trigger, ext trigger to software trigger.
	ADC1->CR2 = 0x9E0000;  
	                      
	// Reset the calibration registers.
	ADC1->CR2 |= 0x09; 
	// Wait for reseting.
	while ((ADC1->CR2 & 0x08) != 0)
	{
		
	} 

	// Switch on autocalibration.
	ADC1->CR2 |= 0x04; 
	// Wait for switching on.
	while ((ADC1->CR2 & 0x04) != 0) 
	{
		
	} 
}

unsigned int analog_input1(unsigned char channel, unsigned char sample_time)
{
	ADC1->SQR3 = 0;
	ADC1->SQR3 = channel;
	
	ADC1->SMPR2 = 0;
	ADC1->SMPR2 |= sample_time << (channel * 3);
	
	// Convertation.
	ADC1->CR2 |= 0x500000;
	// Wait for convertation.
	while ((ADC1->SR & 0x02) == 0)  
	{
		
	}
	
	return ADC1->DR;
}

void analog_output1(int value)
{
	DAC->DHR12R1 = value;
}
