#include "string.h"

void uint2string(unsigned int n, char buffer[UINT_MAX_LENGTH])
{	
	ullong2string((unsigned long long int)n, buffer);
}

void ullong2string(unsigned long long int n, char buffer[ULLONG_MAX_LENGTH])
{
	if (n == 0)
	{
		buffer[0] = 0x30;
		buffer[1] = '\0';
	}
	else
	{
		char local_buffer[ULLONG_MAX_LENGTH];
		int number_of_digits = 0;
		
		while (n > 0)
		{
			int last_digit = n % 10;
			
			local_buffer[number_of_digits] = last_digit + 0x30;
			
			n /= 10;
			
			number_of_digits++;
		}
		
		{
			int i, j;
			for (i = 0, j = number_of_digits - 1; i < number_of_digits; i++, j--)
			{
				buffer[i] = local_buffer[j];
			}
		}
		
		buffer[number_of_digits] = '\0';
	}
}

void int2string(int n, char buffer[INT_MAX_LENGTH])
{
	if (n < 0)
	{
		buffer[0] = '-';
		uint2string(-n, buffer + 1);
	}
	else
	{
		uint2string(n, buffer);
	}
}
