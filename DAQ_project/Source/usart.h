#ifndef USART_H
#define USART_H

#include "stm32f10x.h"

void USART_configuration(USART_TypeDef* USARTx);
void USART_send_byte(USART_TypeDef* USARTx, uint16_t data);
void USART_send_string(USART_TypeDef* USARTx, const char* string);
/*
 * true if UART received data, the data is written into the passed pointer.
 * false if UART has no data received, the passed parameter is not changed.
*/
bool USART_get_received_data(USART_TypeDef* USARTx, uint8_t* data_byte);

#endif // USART_H
