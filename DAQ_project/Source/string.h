#ifndef STRING_H
#define STRING_H

#define UINT_MAX_LENGTH 	13
#define ULLONG_MAX_LENGTH	20
#define INT_MAX_LENGTH 		13

void uint2string(unsigned int n, char buffer[UINT_MAX_LENGTH]);
void ullong2string(unsigned long long int n, char buffer[ULLONG_MAX_LENGTH]);
void int2string(int n, char buffer[INT_MAX_LENGTH]);

#endif // STRING_H
