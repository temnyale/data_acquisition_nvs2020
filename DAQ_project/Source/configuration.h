#ifndef DAQ_CONFIGURATION_H
#define DAQ_CONFIGURATION_H

#include "stm32f10x.h"

#define USE_UART
#define PRINT_TICKS

#define USART_ENGAGED USART1

// Strings. - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#define GREETING_S					"Hello."
#define ECHO_S	    				"echo: "
#define NL_CR_S						"\n\r"
#define	TICKS_S						"Number of ticks from start: "
#define	ADC1_S						"ADC1 value: "
#define	ADC2_S						"ADC2 value: "
#define	ADC3_S						"ADC3 value: "
#define	ADC6_S						"ADC6 value: "
#define	DAC1_S						"DAC1 value: "
#define	LIGHTING_S					"Lighting: "
#define	PERCENT_S					"%"
#define	PHOTOTRANSISTOR_BLOCKED_S 	"The number of times the phototransistor was blocked: "
#define	TEMPERATURE_S 				"Temperature: "
#define	VOLTAGE_SENSOR_S 			"V_sensor: "
#define	ANGLE_S 					"Angle: "
#define	PWM_FREQUENCY_S 			"PWM frequency: "
#define	PWM_DUTY_CYCLE_S 			"PWM duty cycle: "
#define START_OF_OUTPUT_S 			"\n\rStart of output - - - - - -\n\r\n\r"
#define END_OF_OUTPUT_S 			"\n\rEnd of output - - - - - -\n\r"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

// User input symbols. - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
#define INCREASE_DAC_SYMBOL 			'q'
#define DECREASE_DAC_SYMBOL 			'w'
#define INCREASE_PWM_FREQUENCY_SYMBOL 	'a'
#define DECREASE_PWM_FREQUENCY_SYMBOL 	's'
#define INCREASE_PWM_DUTY_CYCLE_SYMBOL 	'd'
#define DECREASE_PWM_DUTY_CYCLE_SYMBOL 	'f'
#define PRINT_STATE_SYMBOL 				'p'
#define CHANGE_OUTPUT_MODE_SYMBOL 		'o'
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

// Constants for PWM. - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
#define PWM_FREQUENCY_MAX 	9999
#define PWM_DUTY_CYCLE_MAX 	9999
#define PWM_FREQUENCY_STEP 	100
#define PWM_DUTY_CYCLE_STEP 100
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

// A constant for FAC. - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#define DAC_VALUE_USER_STEP 100
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

// Constants for the input from the potentiometer. - - - - - - - - - - - - - - - - - - - - -
// The maximum value that can be read from the potentiometer.
#define POTENTIOMETER_MAX 4095
// The minimum value that can be read from the potentiometer.
#define POTENTIOMETER_MIN 550
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

// If the value from the phototransitor is less than PHOTOTRANSISTOR_BLOCKED_THRESHOLD,
// the program consider that there is an object between the phototransitor and the LED.
#define PHOTOTRANSISTOR_BLOCKED_THRESHOLD 4000

// Every OUTPUT_FREQUENCY the program prints its state if it's using UART.
#define OUTPUT_FREQUENCY 100000

// Constants for the RGB LED. - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// Voltages for colors.
#define RGB_LED_R_MIN 				0.0f
#define RGB_LED_R_MAX 				0.0625f
#define RGB_LED_G_MIN 				0.4375f
#define RGB_LED_G_MAX 				0.5625f
#define RGB_LED_B_MIN 				0.9375f
#define RGB_LED_B_MAX 				1.0f
#define RGB_LED_R 					(unsigned char)(~0x80)
#define RGB_LED_G 					(unsigned char)(~0x20)
#define RGB_LED_B 					(unsigned char)(~0x40)
#define RGB_LED_OFF					(unsigned char)(0xE0)
#define RGB_LED_AND					&
#define RGB_LED_UPDATE_FREQUENCY	10000
#define RGB_LED_TEST_DELAY			1000000
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

// Constants for the shift register. - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#define SHIFT_REGISTER_GATEWAY 		GPIOB->ODR
#define SHIFT_REGISTER_RESET_BIT 	5
#define SHIFT_REGISTER_CLOCK_BIT 	6
#define SHIFT_REGISTER_DATA_BIT 	7
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

#endif // DAQ_CONFIGURATION_H
