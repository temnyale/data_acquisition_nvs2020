#include "rcc.h"
#include "stm32f10x.h"

void RCC_configuration(void)
{
	RCC->CR |= 0x10000; //HSE on
	while(!(RCC->CR & 0x20000)) {}
	//flash access setup
	FLASH->ACR &= 0x00000038;   //mask register
	FLASH->ACR |= 0x00000002;   //flash 2 wait state

	FLASH->ACR &= 0xFFFFFFEF;   //mask register
	FLASH->ACR |= 0x00000010;   //enable Prefetch Buffer

	RCC->CFGR &= 0xFFC3FFFF; //maskovani PLLMUL
	RCC->CFGR |= 0x1 << 18; //Nastveni PLLMUL 3x
	RCC->CFGR |= 0x0 << 17; //nastaveni PREDIV1 1x
	RCC->CFGR |= 0x10000; //PLL bude clocovan z PREDIV1
	RCC->CFGR &= 0xFFFFFF0F; //HPRE=1x
	RCC->CFGR &= 0xFFFFF8FF; //PPRE2=1x
	RCC->CFGR &= 0xFFFFC7FF; //PPRE2=1x

	RCC->CFGR |= 0x2; // HSE.
		
	RCC->CR |= 0x01000000; //PLL on
	while(!(RCC->CR & 0x02000000)) {} //PLL stable??

	RCC->CFGR &= 0xFFFFFFFC;
	RCC->CFGR |= 0x2; //nastaveni PLL jako zdroj hodin pro SYSCLK

	while(!(RCC->CFGR & 0x00000008))   //je SYSCLK nastaveno?
	{
		
	}	
}
